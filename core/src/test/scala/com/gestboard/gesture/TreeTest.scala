package com.gestboard.gesture

import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class TreeTest extends WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory {

  "Empty tree" should {
    "be created from empty list of letters" in {
      Tree("", 4) should === (Leaf(None))
    }
  }

  "One level Tree" should {
    "be created from not full list of letters" in {
      Tree("ab", 4) should === (Node(None, List(Leaf(Some('a')), Leaf(Some('b')))))
      Tree("abc", 4) should === (Node(None, List(Leaf(Some('a')), Leaf(Some('b')), Leaf(Some('c')))))
    }
    "be created from list of letters" in {
      Tree("abcd", 4) should === (Node(None, List(Leaf(Some('a')), Leaf(Some('b')), Leaf(Some('c')), Leaf(Some('d')))))
    }
  }

  "Multi level Tree" should {
    "be created from bigger list" in {
      Tree("abcdefgh", 4) should === (
        Node(
          None,
          List(
            Node(
              Some('a'),
              "efgh".toList.map(i => Leaf(Option(i)))
            ),
            Leaf(Some('b')),
            Leaf(Some('c')),
            Leaf(Some('d'))
          )
        )
      )
      Tree("abcdefgh", 3) should === (
        Node(
          None,
          List(
            Node(
              Some('a'),
              "def".toList.map(i => Leaf(Option(i)))
            ),
            Node(
              Some('b'),
              "gh".toList.map(i => Leaf(Option(i)))
            ),
            Leaf(
              Some('c')
            )
          )
        )
      )
      Tree("abcdefgh", 2) should === (
        Node(
          None,
          List(
            Node(
              Some('a'),
              List(
                Node(Some('c'), "gh".toList.map(i => Leaf(Option(i)))),
                Leaf(Some('d'))
              )
            ),
            Node(
              Some('b'),
              "ef".toList.map(i => Leaf(Option(i)))
            )
          )
        )
      )
    }
  }

  "Tree search" should {
    "find leaf by index" in {
      Tree("a", 4).at(1) should === (None)
      Tree("a", 4).at(0) should === (Option(Leaf(Some('a'))))
      Tree("bc", 2).at(1) should === (Option(Leaf(Some('c'))))
      Tree("de", 2).at(10) should === (None)
    }
  }
}

