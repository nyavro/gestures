package com.gestboard.gesture

import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import rx.lang.scala.Observable

class GestureStreamTest extends WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory {

  "GestureStream" should {
    "translate stream of directions into stream of gestures: first level" in {
      new GestureStream(Tree("abcd", 4), Observable.from(List(0, 1, 2, 3)))
        .detect().toList.toBlocking.single should === (List('a', 'b', 'c', 'd'))
    }
    "translate stream of directions into stream of gestures: higher level" in {
      new GestureStream(Tree("abcefghijklm", 3), Observable.from(List(0, 0, 0, 1, 0, 2, 1, 0, 1, 1, 1, 2, 2, 0, 2, 1, 2, 2)))
        .detect().toList.toBlocking.single should === ("efghijklm".toList)
    }
    "treat negative strokes as sequence terminators" in {
      new GestureStream(
        Tree("abcefghijklmno", 3),
        Observable.from(
          List(0, -1, 1, -1, 2, -1, 0, 0, -1, 0, 1, -1, 0, 2, -1, 1, 0, -1, 1, 1, -1, 1, 2, -1, 2, 0, -1, 2, 1, -1, 2, 2, -1, 0, 0, 0, -1, 0, 0, 1, -1)
        )
      )
        .detect().toList.toBlocking.single should === ("abcefghijklmno".toList)
    }
    "ignore incomplete sequences" in {
      val tree = Tree("jklmnopqrstuvw", 2)
      new GestureStream(tree, Observable.from(List(0))).detect().toList.toBlocking.single should === (List())
      new GestureStream(tree, Observable.from(List(0, 0))).detect().toList.toBlocking.single should === (List())
      new GestureStream(tree, Observable.from(List(0, 1))).detect().toList.toBlocking.single should === (List())
      new GestureStream(tree, Observable.from(List(1))).detect().toList.toBlocking.single should === (List())
      new GestureStream(tree, Observable.from(List(1, 0))).detect().toList.toBlocking.single should === (List())
      new GestureStream(tree, Observable.from(List(1, 1))).detect().toList.toBlocking.single should === (List())
    }
  }
}

