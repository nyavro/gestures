package com.gestboard.gesture

import com.gestboard.geometry.Point
import com.gestboard.geometry.areas.Areas
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import rx.lang.scala.Observable

class StrokeStreamTest extends WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory {

  "StrokeStream" should {
    "ignore first point" in {
       new StrokeStream(
         new Areas[Int]{
           override def ofPoint(p: Point): Option[Int] = Option(1)
         },
         Observable.from(List(Point(1.0, 2.0)))
       ).detect().toList.toBlocking.single should === (List())
    }
    "detect 1 stroke" in {
      new StrokeStream(
        new Areas[Int]{
          override def ofPoint(p: Point): Option[Int] = Option(2)
        },
        Observable.from(List(Point(3.0, 4.0), Point(5.0, 6.0)))
      ).detect().toList.toBlocking.single should === (List(2))
    }
    "detect 2 stroke" in {
      new StrokeStream(
        new Areas[Int]{
          var i = 0
          override def ofPoint(p: Point): Option[Int] = {
            i = i+1
            Option(i)
          }
        },
        Observable.from(List(Point(7.0, 8.0), Point(9.0, 10.0), Point(10.0, 11.0)))
      ).detect().toList.toBlocking.single should === (List(1, 2))
    }
    "detect 3 stroke" in {
      new StrokeStream(
        new Areas[Int]{
          var i = 0
          override def ofPoint(p: Point): Option[Int] = {
            i = i+1
            Option(i)
          }
        },
        Observable.from(List(Point(7.1, 8.1), Point(9.1, 10.1), Point(10.1, 11.1), Point(12.1, 13.1)))
      ).detect().toList.toBlocking.single should === (List(1, 2, 3))
    }
    "ignore not detected strokes" in {
      new StrokeStream(
        new Areas[Int]{
          var i = 0
          override def ofPoint(p: Point): Option[Int] = {
            i = i+1
            Option(i).filter(_%2==0)
          }
        },
        Observable.from(List(Point(7.2, 8.2), Point(9.2, 10.2), Point(10.2, 11.2), Point(12.2, 13.2), Point(14.2, 15.2)))
      ).detect().toList.toBlocking.single should === (List(2, 4))
    }
  }
}

