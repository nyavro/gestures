package com.gestboard.detection

import com.gestboard.geometry.Point
import com.gestboard.geometry.areas.SafeRing
import com.gestboard.stream.{START, TouchEventsStream}
import org.scalacheck.Prop.forAll
import org.scalacheck._
import rx.lang.scala.Observable

object TouchEventStreamSpecification extends Properties("TouchEventsStream") with PointsGenerators {

  val R = 10.0

  def stroke(r:Double) = for {
    start <- point
    in <- Gen.nonEmptyListOf(inCircle(start, r))
    out <- outOfCircle(start, r)
  } yield start::in ++ List(out)

  def strokes(start:Point, r:Double, n:Int):Gen[List[Point]] = if (n==0) {
    Gen.const(List[Point]())
  } else {
    for {
      in <- Gen.listOfN(9, inCircle(start, r))
      out <- outOfCircle(start, r)
      rest <- strokes(out, r, n-1)
    } yield start :: in ++ rest
  }

  property("StartEvent") = forAll(inCirclePoints(R)) {
    points => new TouchEventsStream(new SafeRing(R), Observable.from(points)).detect().toList.toBlocking.single == List(START(points.head))
  }

  property("GotStroke") = forAll(stroke(R)) {
    points => new TouchEventsStream(new SafeRing(R), Observable.from(points)).detect().toList.toBlocking.single == List(START(points.head), START(points.last))
  }

  property("EndOfStrokeStartsNewStroke") = forAll(strokes(Point.zero, R, 10)) {
    points =>
      new TouchEventsStream(new SafeRing(R), Observable.from(points)).detect().toList.toBlocking.single ==
        points
          .zipWithIndex
          .collect {
            case (p, i) if i % 10 == 0 => START(p)
          }
  }
}
