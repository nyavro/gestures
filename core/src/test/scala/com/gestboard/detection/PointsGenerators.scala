package com.gestboard.detection

import com.gestboard.geometry.Point
import org.scalacheck.Gen

trait PointsGenerators {

  val Range = 1000.0

  def onCircle(r:Double) = for {
    x <- Gen.choose(-r, r)
    y = Math.sqrt(r*r - x*x)
  } yield Point(x, y)

  def inCircle(start:Point, r:Double) = for {
    p <- onCircle(r)
    l <- Gen.choose(0.0, 1.0)
  } yield start + p*l

  def outOfCircle(start:Point, r:Double):Gen[Point] = for {
    point <- point
    res <- {
      if (point.distanceTo(start) > r) {
        Gen.const(point)
      } else {
        outOfCircle(start, r)
      }
    }
  } yield res

  def point = for {
    x <- Gen.choose(-Range, Range)
    y <- Gen.choose(-Range, Range)
  } yield Point(x, y)

  def inCirclePoints(r:Double) = for {
    start <- point
    points <- Gen.nonEmptyListOf(inCircle(start, r))
  } yield start::points

  def angle = Gen.choose(0.0, Math.PI*2.0)
}
