package com.gestboard.geometry

import com.gestboard.detection.PointsGenerators
import org.scalacheck.Prop._
import org.scalacheck.Properties

object PointSpecification extends Properties("Point") with PointsGenerators {

  val E = 0.00001

  property("AngleCalculation") = forAll(angle) {
    angle => new Point(1.0, 0.0).rotate(angle).angle.exists(v => Math.abs(v - angle) < E)
  }

}
