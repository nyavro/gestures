package com.gestboard.geometry.areas

import com.gestboard.detection.PointsGenerators
import com.gestboard.geometry.Point
import org.scalacheck.Prop._
import org.scalacheck._

object SectorsSpecification extends Properties("Sectorization") with PointsGenerators {

  def normalPointInSector(from:Double, to:Double) = {
    for {
      t <- Gen.choose(0.0, 1.0)
    } yield Point(1.0, 0.0).rotate(from + (to - from)*t)
  }

  def pointInSector(from:Double, to:Double) = {
    for {
      pt <- normalPointInSector(from, to)
      l <- Gen.choose(0.0, Range)
    } yield pt*l
  }

  property("Zero-segment circle retrurns None for any input") = forAll(point) {
    point => new Sectors(0).ofPoint(point).isEmpty
  }

  property("Returns Some(0) for single sector") = forAll(point) {
    point => new Sectors(1).ofPoint(point).contains(0)
  }

  property("2 sector circle: returns Some(0) for y > 0") = forAll(point.filter(_.y >= 0)) {
    point => new Sectors(2).ofPoint(point).contains(0)
  }

  property("2 sector circle: returns Some(1) for y < 0") = forAll(point.filter(_.y < 0)) {
    point => new Sectors(2).ofPoint(point).contains(1)
  }

  property("Point in sector") = forAll(
    for {
      at <- angle
      bt <- angle
      a = Math.min(at, bt)
      b = Math.max(at, bt)
      pt <- pointInSector(a, b)
    } yield (a, b, pt)
  ) {
    case (a, b, pt) => pt.angle.exists(v => v <= b && v >= a)
  }

  property("3 sector circle: returns Some(0) for point in first sector") = forAll(
    for {
      point <- normalPointInSector(0.0, 2.0*Math.PI/3.0)
      l <- Gen.choose(2.0, 10.0)
    } yield point*l
  )(
    point => {
      val circle: Sectors = new Sectors(3)
      val of: Option[Int] = circle.ofPoint(point)
      of.contains(0)
    }
  )

  property("3 sector circle: returns Some(1) for point in second sector") = forAll(
    for {
      point <- normalPointInSector(2.0*Math.PI/3.0, 4.0*Math.PI/3.0)
      l <- Gen.choose(2.0, 10.0)
    } yield point*l
  )(
    point => {
      val circle: Sectors = new Sectors(3)
      val of: Option[Int] = circle.ofPoint(point)
      of.contains(1)
    }
  )

  property("3 sector circle: returns Some(2) for point in third sector") = forAll(
    for {
      point <- normalPointInSector(4.0*Math.PI/3.0, 6.0*Math.PI/3.0)
      l <- Gen.choose(2.0, 10.0)
    } yield point*l
  )(
    point => {
      val circle: Sectors = new Sectors(3)
      val of: Option[Int] = circle.ofPoint(point)
      of.contains(2)
    }
  )

  property("4 sector circle: returns Some(0) for point in first sector") = forAll(
    for {
      point <- normalPointInSector(0.0, 2.0*Math.PI/4.0)
      l <- Gen.choose(2.0, 10.0)
    } yield point*l
  )(
    point => {
      val circle: Sectors = new Sectors(4)
      val of: Option[Int] = circle.ofPoint(point)
      of.contains(0)
    }
  )

  property("4 sector circle: returns Some(1) for point in second sector") = forAll(
    for {
      point <- normalPointInSector(2.0*Math.PI/4.0, 4.0*Math.PI/4.0)
      l <- Gen.choose(2.0, 10.0)
    } yield point*l
  )(
    point => {
      val circle: Sectors = new Sectors(4)
      val of: Option[Int] = circle.ofPoint(point)
      of.contains(1)
    }
  )

  property("4 sector circle: returns Some(2) for point in third sector") = forAll(
    for {
      point <- normalPointInSector(4.0*Math.PI/4.0, 6.0*Math.PI/4.0)
      l <- Gen.choose(2.0, 10.0)
    } yield point*l
  )(
    point => {
      val circle: Sectors = new Sectors(4)
      val of: Option[Int] = circle.ofPoint(point)
      of.contains(2)
    }
  )

  property("4 sector circle: returns Some(3) for point in fourth sector") = forAll(
    for {
      point <- normalPointInSector(6.0*Math.PI/4.0, 8.0*Math.PI/4.0)
      l <- Gen.choose(2.0, 10.0)
    } yield point*l
  )(
    point => {
      val circle: Sectors = new Sectors(4)
      val of: Option[Int] = circle.ofPoint(point)
      of.contains(3)
    }
  )
}
