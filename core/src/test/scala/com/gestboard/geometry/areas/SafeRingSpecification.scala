package com.gestboard.geometry.areas

import com.gestboard.detection.PointsGenerators
import com.gestboard.geometry.Point
import org.scalacheck.Prop._
import org.scalacheck._

object SafeRingSpecification extends Properties("SafeRing") with PointsGenerators {

  val R = 10.0

  property("NoEventsInSafeRing") = forAll(inCircle(Point.zero, R)) {
    point => new SafeRing(R).ofPoint(point).isEmpty
  }

  property("NoEventsInSafeRing") = forAll(outOfCircle(Point.zero, R)) {
    point => new SafeRing(R).ofPoint(point).contains(true)
  }
}
