package com.gestboard.geometry.areas

import com.gestboard.detection.PointsGenerators
import com.gestboard.geometry.Point
import org.scalacheck.Prop._
import org.scalacheck._

object SafeRingSectorsSpecification extends Properties("SafeRingSectors") with PointsGenerators {

  val R = 10.0

  def normalPointInSector(from:Double, to:Double) = {
    for {
      t <- Gen.choose(0.0, 1.0)
    } yield Point(1.0, 0.0).rotate(from + (to - from)*t)
  }

  def pointInSector(from:Double, to:Double) = {
    for {
      pt <- normalPointInSector(from, to)
      l <- Gen.choose(0.0, Range)
    } yield pt*(l*R)
  }

  property("None for points inside safe circle") = forAll(
    for {
      circle <- onCircle(R)
      r <- Gen.choose(0.0, 1.0)
      n <- Gen.choose(0, 100)
    } yield (circle*r, n)
  ) {
    case (p:Point, n:Int) => new SafeRingSectors(new SafeRing(R), new Sectors(n)).ofPoint(p).isEmpty
  }

  property("Returns Option(index) for point outside safe circle") = forAll(
    for {
      circle <- onCircle(R)
      r <- Gen.choose(1.0, Range)
      n <- Gen.choose(1, 100)
    } yield (circle*r, n)
  ) {
    case (p, n) => new SafeRingSectors(new SafeRing(R), new Sectors(n)).ofPoint(p).exists(_<n)
  }
}
