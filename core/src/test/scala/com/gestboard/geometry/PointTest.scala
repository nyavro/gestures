package com.gestboard.geometry

import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class PointTest extends WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory {

  val Pi2 = 2*Math.PI
  val E = 0.00001

  "Point" should {
    "rotate itself on angle" in {
      val point = new Point(1.0, 0)
      val angle: Double = Pi2 / 10.0
      (1 to 10).foreach {
        i => point.rotate(i*angle).angle.foreach(_ should === (i*angle +- E))
      }
    }
  }
}

