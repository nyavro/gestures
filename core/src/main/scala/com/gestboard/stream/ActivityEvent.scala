package com.gestboard.stream

import com.gestboard.geometry.Point

sealed trait ActivityEvent

case class START(at:Point) extends ActivityEvent
