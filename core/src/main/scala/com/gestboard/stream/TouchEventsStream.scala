package com.gestboard.stream

import com.gestboard.geometry.Point
import com.gestboard.geometry.areas.{Areas, MovedArea}
import rx.lang.scala.Observable

class TouchEventsStream[A](areas:Areas[A], source:Observable[Point]) {

  def detect() = source
    .scan(Option.empty[(Areas[_], Option[Point])]) {
      case (Some((area, _)), cur) if area.ofPoint(cur).isEmpty => Some((area, None))
      case (_, cur) => Some((new MovedArea(cur, areas), Option(cur)))
    }
    .collect {
      case Some((_, Some(start))) => START(start)
    }
}
