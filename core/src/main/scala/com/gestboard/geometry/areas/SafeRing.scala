package com.gestboard.geometry.areas

import com.gestboard.geometry.Point

class SafeRing(r:Double) extends Areas[Boolean]{
  override def ofPoint(p: Point): Option[Boolean] = {
    if (p.norm <= r) None
    else Some(true)
  }
}
