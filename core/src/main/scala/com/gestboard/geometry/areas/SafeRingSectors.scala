package com.gestboard.geometry.areas

import com.gestboard.geometry.Point

class SafeRingSectors(safeRing: Areas[Boolean], sectors:Areas[Int]) extends Areas[Int] {

  override def ofPoint(p: Point): Option[Int] =
    for {
      outOfSafe <- safeRing(p)
      index <- sectors(p)
    } yield index
}
