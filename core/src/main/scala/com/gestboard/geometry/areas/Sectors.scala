package com.gestboard.geometry.areas

import com.gestboard.geometry.Point

class Sectors(count:Int) extends Areas[Int] {

  override def ofPoint(p:Point):Option[Int] = count match {
    case 0 => None
    case 1 => Some(0)
    case _ => p.angle.map(angleToIndex)
  }

  private def angleToIndex (d:Double) = {
    (d * 0.5 * count / Math.PI).toInt
  }
}
