package com.gestboard.geometry.areas

import com.gestboard.geometry.Point

class MovedArea[A](center:Point, inner: Areas[A]) extends Areas[A] {

  override def ofPoint(p: Point) = {
    inner(p - center)
  }
}
