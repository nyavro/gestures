package com.gestboard.geometry.areas

import com.gestboard.geometry.Point

trait Areas[A]{

  def ofPoint(p:Point):Option[A]

  def apply(p:Point) = ofPoint(p)
}

