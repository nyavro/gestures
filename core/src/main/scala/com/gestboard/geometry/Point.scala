package com.gestboard.geometry

case class Point(x:Double, y:Double) {

  def angle:Option[Double] = {
    val len = norm
    val cos = if (len > 0.0) Option(x/len) else None
    cos.map (
      v => {
        val acos = Math.acos(v)
        if (y > 0.0)
          acos
        else
          Math.PI * 2.0 - acos
      }
    )
  }

  def rotate(angle: Double) = {
    val ang = if (angle > Math.PI) angle-Math.PI*2.0 else angle
    val cos = Math.cos(ang)
    val sin = Math.sin(ang)
    Point(x*cos-y*sin, x*sin + y*cos)
  }

  def distanceTo(p:Point):Double = (this - p).norm

  def -(p:Point):Point = Point(x-p.x, y-p.y)

  def +(p:Point):Point = Point(x+p.x, y+p.y)

  def *(p:Point):Double = x*p.x+y*p.y

  def *(d:Double):Point = Point(x*d, y*d)

  def norm:Double = Math.sqrt(x*x+y*y)

}

object Point {
  val zero = new Point(0.0, 0.0)
  implicit def pairToPoint(pair:(Int, Int)):Point = Point(pair._1, pair._2)
  implicit def pairToPoint2(pair:(Double, Double)):Point = Point(pair._1, pair._2)
}