package com.gestboard.gesture

import rx.lang.scala.Observable

class GestureStream(tree:Tree[Char], source:Observable[Int]) {
  def detect() = source
    .scan((tree, Option.empty[Char])) {
      case ((tr, _), cur) =>
        if (cur < 0) {
          (tree, tr.value)
        }
        else tr.at(cur) match {
          case Some(Leaf(value)) => (tree, value)
          case Some(node) => (node, None)
          case None => (tree, None)
        }
    }
    .collect {
      case (_, Some(ch)) => ch
    }
}
