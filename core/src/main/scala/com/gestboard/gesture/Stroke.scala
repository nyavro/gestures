package com.gestboard.gesture

sealed trait Stroke

case object UP extends Stroke
case object DOWN extends Stroke
case object OTHER_TMP extends Stroke
