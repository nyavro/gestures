package com.gestboard.gesture

import com.gestboard.geometry.Point
import com.gestboard.geometry.areas.{Areas, MovedArea}
import rx.lang.scala.Observable

class StrokeStream(areas:Areas[Int], source:Observable[Point]) {

  def detect():Observable[Int] = source
    .scan(Option.empty[(Areas[Int], Option[Int])]) {
      case (Some((area, _)), cur) => Some((area, area.ofPoint(cur)))
      case (_, cur) => Some((new MovedArea(cur, areas), None))
    }
    .collect {
      case Some((_, Some(stroke))) => stroke
    }
}
