package com.gestboard.gesture

trait Tree [A] {
  def at(stroke:Int):Option[Tree[A]]

  def value:Option[A]
}

case class Leaf[A](value:Option[A]) extends Tree[A] {
  override def at(stroke:Int) = None
}
case class Node[A](value:Option[A], children:List[Tree[A]]) extends Tree[A] {
  override def at(stroke:Int) =
    if(stroke < children.size) Option(children(stroke))
    else None
}

object Tree {

  private def toTree[A](groups:List[List[A]], size:Int):Tree[A] = groups match {
    case x::xs => Node(None, genTree(xs, x.map(i => Leaf(Option(i))), size))
    case Nil => Leaf(None)
  }

  private def genTree[A](groups:List[List[A]], layer:List[Tree[A]], size:Int):List[Tree[A]] =
    groups.foldLeft(layer) {
      case (lr, item) =>
        item.map(Option(_)).zipAll(lr.grouped(size).toList, None, List.empty[Tree[A]]).map {
          case (value, Nil) => Leaf(value)
          case (value, b) => Node(value, b)
        }
    }

  private def progressiveGroup[A](list:List[A], size:Int) = list
    .foldLeft((size, List.empty[List[A]])) {
      case ((n, Nil), v) => (n, List(v)::Nil)
      case ((n, x::xs), v) if x.size < n => (n, (v::x)::xs)
      case ((n, x::xs), v) => (n*size, List(v)::x::xs)
    }
    ._2
    .map(_.reverse)

  def apply(letters:String, size:Int):Tree[Char] =
    toTree(
      progressiveGroup(
        letters.toList,
        size
      ),
      size
    )
}

