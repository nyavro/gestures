name := "gestures-core"

version := "1.0"

scalaVersion := "2.11.8"

val specs2V = "3.8.3"
val scalaTestV = "2.2.6"
val scalaMockV = "3.2.2"
val rxScalaV = "0.26.2"
val scalacheckV = "1.13.2"

libraryDependencies ++= Seq(
  "io.reactivex" %% "rxscala" % rxScalaV,
  "org.scalacheck" %% "scalacheck" % scalacheckV % "test",
  "org.specs2" %% "specs2-core" % specs2V % "test",
  "org.scalatest" %% "scalatest" % scalaTestV % "test",
  "org.scalamock" %% "scalamock-scalatest-support" % scalaMockV % "test"
)

exportJars := true