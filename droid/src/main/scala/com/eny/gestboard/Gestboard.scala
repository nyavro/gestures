package com.eny.gestboard

import android.content.Context
import android.inputmethodservice.InputMethodService
import android.util.Log
import android.view.{MotionEvent, View}
import android.view.inputmethod.InputMethodManager

class Gestboard extends InputMethodService {

  private var inputMethodManager:InputMethodManager = _

  override def onCreate() = {
    super.onCreate()
    inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE).asInstanceOf[InputMethodManager]
  }

  override def onCreateInputView: View = getLayoutInflater.inflate(R.layout.input, null).asInstanceOf[GestboardView]

  override def onGenericMotionEvent(event: MotionEvent): Boolean = {
    Log.d("Gestboard", s"onGenericMotionEvent ${event.getX} : ${event.getY}")
    true
  }
}

