package com.eny.gestboard

import android.content.Context
import android.graphics._
import android.util.{AttributeSet, Log}
import android.view.{MotionEvent, View}
import com.gestboard.geometry.Point
import rx.lang.scala.subjects.PublishSubject

class GestboardView(context: Context, attrs:AttributeSet)
  extends View(context, attrs) {

  val paint = new Paint
  val str = new Paint
  paint.setColor(Color.RED)
  str.setColor(Color.GREEN)
  val blue = new Paint
  blue.setColor(Color.BLUE)

  private lazy val canvas: Canvas = new Canvas(buffer)

  private lazy val buffer: Bitmap = Bitmap.createBitmap(Math.max(1, getWidth), Math.max(1, getHeight), Bitmap.Config.ARGB_8888)

  private val sub = PublishSubject[Option[Point]]()

  sub
    .zip(sub.tail)
    .subscribe(
      {
        case (None, Some(cur)) => drawStart(cur)
        case (Some(prev), Some(cur)) => drawMove(prev, cur)
        case (Some(prev), None) => drawEnd(prev)
      },
      err => {Log.d("GestBoardObservable", err.getLocalizedMessage, err)},
      () => {}
    )

  def drawStart(start:Point) = {
    canvas.drawCircle(start.x.toFloat, start.y.toFloat, 50.0f, paint)
    invalidate()
  }

  def drawEnd(end:Point) = {
    canvas.drawCircle(end.x.toFloat, end.y.toFloat, 40.0f, blue)
    invalidate()
  }

  def drawMove(prev: Point, cur:Point) = {
    canvas.drawLine(prev.x.toFloat, prev.y.toFloat, cur.x.toFloat, cur.y.toFloat, str)
    invalidate()
  }

  override def onDraw(canvas: Canvas) = {
    super.onDraw(canvas)
    canvas.drawBitmap(buffer, 0, 0, null)
  }

  override def onTouchEvent(me: MotionEvent):Boolean = {
    if (me.getAction == MotionEvent.ACTION_DOWN || me.getAction == MotionEvent.ACTION_MOVE)
      sub.onNext(Option(Point(me.getX, me.getY())))
    else if (me.getAction == MotionEvent.ACTION_UP)
      sub.onNext(None)
    true
  }
}
