package com.eny.gestboard

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.preference.PreferenceFragment

class InputMethodSettingsFragment extends PreferenceFragment with InputMethodSettings {

  override def setInputMethodSettingsCategoryTitle(resId: Int) = mSettings.setInputMethodSettingsCategoryTitle(resId)

  override def setInputMethodSettingsCategoryTitle(title: CharSequence) = mSettings.setInputMethodSettingsCategoryTitle(title)

  override def setSubtypeEnablerTitle(resId: Int) = mSettings.setSubtypeEnablerTitle(resId)

  override def setSubtypeEnablerTitle(title: CharSequence) = mSettings.setSubtypeEnablerTitle(title)

  override def setSubtypeEnablerIcon(resId: Int) = mSettings.setSubtypeEnablerIcon(resId)

  override def setSubtypeEnablerIcon(drawable: Drawable) = mSettings.setSubtypeEnablerIcon(drawable)

  private val mSettings = new InputMethodSettingsImpl()

  override def onResume() = {
    super.onResume()
    mSettings.updateSubtypeEnabler()
  }

  override def onCreate(savedInstanceState: Bundle) = {
    super.onCreate(savedInstanceState)
    setPreferenceScreen(getPreferenceManager.createPreferenceScreen(getActivity))
    mSettings.init(getActivity, getPreferenceScreen)
    setInputMethodSettingsCategoryTitle(R.string.language_selection_title)
    setSubtypeEnablerTitle(R.string.select_language)
    addPreferencesFromResource(R.xml.ime_preferences)
  }
}
