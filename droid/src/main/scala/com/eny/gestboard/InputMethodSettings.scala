package com.eny.gestboard

import android.graphics.drawable.Drawable

trait InputMethodSettings {
  def setInputMethodSettingsCategoryTitle(resId:Int):Unit
  def setInputMethodSettingsCategoryTitle(title:CharSequence)
  def setSubtypeEnablerTitle(resId:Int)
  def setSubtypeEnablerTitle(title:CharSequence)
  def setSubtypeEnablerIcon(resId:Int)
  def setSubtypeEnablerIcon(drawable:Drawable)
}

