package com.eny.gestboard
import android.content.{Context, Intent}
import android.graphics.drawable.Drawable
import android.preference.Preference.OnPreferenceClickListener
import android.preference.{Preference, PreferenceScreen}
import android.provider.Settings
import android.text.TextUtils
import android.view.inputmethod.{InputMethodInfo, InputMethodManager}

import scala.collection.JavaConversions._

class InputMethodSettingsImpl extends InputMethodSettings {
  private var mSubtypeEnablerPreference: Preference = _
  private var mInputMethodSettingsCategoryTitleRes:Int = _
  private var mInputMethodSettingsCategoryTitle:CharSequence = _
  private var mSubtypeEnablerTitleRes:Int = _
  private var mSubtypeEnablerTitle:CharSequence = _
  private var mSubtypeEnablerIconRes:Int = _
  private var mSubtypeEnablerIcon:Drawable = _
  private var mImm:Option[InputMethodManager] = None
  private var mImi:Option[InputMethodInfo] = None
  private var mContext:Option[Context] = None

  def init(context:Context, prefScreen:PreferenceScreen) {
    mContext = Option(context)
    mImm = Option(context.getSystemService(Context.INPUT_METHOD_SERVICE).asInstanceOf[InputMethodManager])
    mImi =
      for {
        imm <- mImm
        ctx <- mContext
        res <- imm.getInputMethodList.find {_.getPackageName==ctx.getPackageName}
      } yield {
        res
      }
    if (mImi.exists(_.getSubtypeCount > 1)) {
      mSubtypeEnablerPreference = new Preference(context)
      mSubtypeEnablerPreference.setOnPreferenceClickListener(
        new OnPreferenceClickListener() {
          override def onPreferenceClick(preference:Preference) = {
            val title = getSubtypeEnablerTitle(context)
            val intent = new Intent(Settings.ACTION_INPUT_METHOD_SUBTYPE_SETTINGS)
            mImi.foreach(imi => intent.putExtra(Settings.EXTRA_INPUT_METHOD_ID, imi.getId))
            if (!TextUtils.isEmpty(title)) {
              intent.putExtra(Intent.EXTRA_TITLE, title)
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
              | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
              | Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent)
            true
          }
        }
      )
      prefScreen.addPreference(mSubtypeEnablerPreference)
      updateSubtypeEnabler()
      true
    }
    else {
      false
    }
  }

  def updateSubtypeEnabler() = {
      if (mSubtypeEnablerPreference != null) {
        if (mSubtypeEnablerTitleRes != 0) {
          mSubtypeEnablerPreference.setTitle(mSubtypeEnablerTitleRes)
        } else if (!TextUtils.isEmpty(mSubtypeEnablerTitle)) {
          mSubtypeEnablerPreference.setTitle(mSubtypeEnablerTitle)
        }
        getEnabledSubtypesLabel.foreach(mSubtypeEnablerPreference.setSummary)
        if (mSubtypeEnablerIconRes != 0) {
          mSubtypeEnablerPreference.setIcon(mSubtypeEnablerIconRes)
        } else if (mSubtypeEnablerIcon != null) {
          mSubtypeEnablerPreference.setIcon(mSubtypeEnablerIcon)
        }
      }
  }

  override def setInputMethodSettingsCategoryTitle(resId: Int) = {
    mInputMethodSettingsCategoryTitleRes = resId
    updateSubtypeEnabler()
  }

  override def setInputMethodSettingsCategoryTitle(title: CharSequence) = {
    mInputMethodSettingsCategoryTitleRes = 0
    mInputMethodSettingsCategoryTitle = title
    updateSubtypeEnabler()
  }

  override def setSubtypeEnablerTitle(resId: Int) = {
    mSubtypeEnablerTitleRes = resId
    updateSubtypeEnabler()
  }

  override def setSubtypeEnablerTitle(title: CharSequence) = {
    mSubtypeEnablerTitleRes = 0
    mSubtypeEnablerTitle = title
    updateSubtypeEnabler()
  }

  override def setSubtypeEnablerIcon(resId: Int) = {
    mSubtypeEnablerIconRes = resId
    updateSubtypeEnabler()
  }

  override def setSubtypeEnablerIcon(drawable: Drawable) = {
    mSubtypeEnablerIconRes = 0
    mSubtypeEnablerIcon = drawable
    updateSubtypeEnabler()
  }

  private def getSubtypeEnablerTitle(context:Context):CharSequence =
    if (mSubtypeEnablerTitleRes != 0)
      context.getString(mSubtypeEnablerTitleRes)
    else
      mSubtypeEnablerTitle

  private def getEnabledSubtypesLabel = {
    for (
      ctx <- mContext;
      imm <- mImm;
      imi <- mImi
    ) yield {
      imm.getEnabledInputMethodSubtypeList(imi, true).map(
        item => item.getDisplayName(ctx, imi.getPackageName, imi.getServiceInfo.applicationInfo)
      ).mkString(", ")
    }
  }
}
