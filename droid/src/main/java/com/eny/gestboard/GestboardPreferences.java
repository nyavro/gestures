package com.eny.gestboard;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class GestboardPreferences extends PreferenceActivity {

    @Override
    public Intent getIntent() {
        final Intent modIntent = new Intent(super.getIntent());
        modIntent.putExtra(EXTRA_SHOW_FRAGMENT, InputMethodSettingsFragment.class.getName());
        modIntent.putExtra(EXTRA_NO_HEADERS, true);
        return modIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // We overwrite the title of the activity, as the default one is "Voice Search".
        setTitle(R.string.settings_name);
    }

    protected boolean isValidFragment(final String fragmentName) {
        return InputMethodSettingsFragment.class.getName().equals(fragmentName);
    }
}
