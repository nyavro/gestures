import sbt.Keys._
import sbt._

object ProjectBuild extends Build {
  lazy val root = Project (
      id = "root",
      base = file ("."),
      settings = super.settings
    )
    .settings(
      name := "Gestboard"
    )
    .aggregate(core, droid)

  lazy val core = project.in(file("core"))
  lazy val droid = project
    .in(file("droid"))
    .dependsOn(core)
}